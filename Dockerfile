from python:3.11

WORKDIR /app

COPY bindapi /app/bindapi/
COPY requirements.txt /app/
RUN  mkdir /app/logs
RUN pip install -r requirements.txt


EXPOSE 8000
CMD ["uvicorn","bindapi:app","--host=0.0.0.0","--port=8000"]

